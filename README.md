The Elevation_segmentation toolbox with the Littoral Zone Surface Area tool was 
created to enable any arcgis user to calculate the littoral zone surface area
of a lake given a certain water level elevation. 
The littoral zone is classified as the area of the water column that has light 
penetration, sufficient for macrophytes to photosynthesis, to reach the sediment
floor of the lake. The analysis was performed using ESRI’s ArcMap and Python 
coding to calculate, automate, and illustrate this relationship; and to provide 
a possible methodology for water and wildlife management to apply to their 
unique situations to make informed decisions in the future. This method is 
advantageous when analyzing present or future conditions because of its versatility
to create hypothetical scenarios. 
Variable lake level is a reality in an ever changing climate and increased water
use. The effect that water level has effects on ecosystems and water quality for 
the amount of littoral zone surface area.
The inputs that the user must specify and outputs for the tool are...
Inputs:
DEM,
Cell Size,
Shapefile to have elevation compared to,
Field of concern,
Boundary Layer,
Top and bottom Elevations,
Littoral Zone depth,
Frequency of measurements,
Output location.

Outputs:
Excel files with the surface area for each field of concern within each littoral zone

Directions for instalation and use include...
downloading the toolbox from this site
adding the tool to ArcGIS 
Running the tool

It is important to consider the resolution of the DEM that you are working with 
and whether it is going to be applicable to the shapefile layer that you are 
relating it to. Also The boundary layer was constructed in order to minimize future
editing and enable the user to have more focused results. This boundary layer 
can include any polygon that encompases the area of concern(specifically the 
lake boundary in this case).