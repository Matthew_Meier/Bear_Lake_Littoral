import arcpy
from arcpy.sa import *


class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Elevation  Comparisons"
        self.alias = "EC"

        # List of tool classes associated with this toolbox
        self.tools = [Littoral_Zone_SA]


class Littoral_Zone_SA(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Littoral Zone Surface Area"
        self.description = "Relates elevation to surface area of a shapefile"
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Define parameter definitions"""
        # in_shp parameter
        param0 = arcpy.Parameter(
            displayName = 'DEM',
            name = 'DEM_Raster',
            datatype = 'GPRasterLayer',
            parameterType = 'Required',
            direction = 'Input')
        # the cellsize or resolution of the DEM that you are working with
        param1 = arcpy.Parameter(
            displayName = 'Cell Size',
            name = 'Resolution',
            datatype= 'GPString',
            parameterType = 'Required',
            direction = 'Input')
        # Shapefile to be cliped parameter
        param2 = arcpy.Parameter(
            displayName = 'Shapefile',
            name = 'shp_fn',
            datatype = 'DEShapefile',
            parameterType = 'Required',
            direction = 'Input')
        # value field that is important in the shapefile
        param3 = arcpy.Parameter(
            displayName = 'Value field of concern in shapefile',
            name = 'Vf',
            datatype = 'GPString',
            parameterType = 'Required',
            direction = 'Input')
        # the boundary layer that is being used to limit the area to be analyzed
        param4 = arcpy.Parameter(
            displayName = 'Boundary',
            name = 'Boundary_shp_fn',
            datatype = 'DEShapefile',
            parameterType = 'Required',
            direction = 'Input')
        # range of elevation desired(this is the possible fluctuation that can occur in the lake)
        param5 = arcpy.Parameter(
            displayName = 'Top Elevation',
            name = 'TE',
            datatype = 'GPLong',
            parameterType = 'Required',
            direction = 'Input')
        param6 = arcpy.Parameter(
            displayName = 'Bottom Elevation',
            name = 'BE',
            datatype = 'GPLong',
            parameterType = 'Required',
            direction = 'Input')
        # the interval size (the actual littoral zone depth that is being observed)
        param7 = arcpy.Parameter(
            displayName = 'Littoral Zone Depth',
            name = 'LZD',
            datatype = 'GPLong',
            parameterType = 'Required',
            direction = 'Input')
        # the steps that are taken between each interval
        param8 = arcpy.Parameter(
            displayName = 'Frequency',
            name = 'Freq',
            datatype = 'GPLong',
            parameterType = 'Required',
            direction = 'Input')
        # the output files location
        param9 = arcpy.Parameter(
            displayName = 'Output Folder',
            name = 'outputloc',
            datatype = 'DEFolder',
            parameterType = 'Required',
            direction = 'Input')
        params = [param0, param1, param2, param3, param4, param5, param6, param7, param8, param9]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        # get parameters
        in_DEM = parameters[0].valueAsText
        cellSize = parameters[1].valueAsText
        in_shpfn = parameters[2].valueAsText
        valField= parameters[3].valueAsText
        boundary_shpfn = parameters[4].valueAsText
        elev_range_TE = parameters[5].valueAsText
        elev_range_BE = parameters[6].valueAsText
        lit_depth = parameters[7].valueAsText
        step_freq = parameters[8].valueAsText
        out_folder = parameters[9].valueAsText
        
        # check out the Arcgis spatial analyst extention license
        arcpy.CheckOutExtension("Spatial")
        messages.addMessage('Clipped DEM by Boundary')

        # clip the DEM using the boundary layer(remember to close the file for the temporary workspace)
        # Set local variables
        out_feature_class = r"C:/temp/studyarea.tif"
        # Execute Clip
        #arcpy.Clip_analysis(in_DEM, boundary_shpfn, out_feature_class)
        # Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
        # clipping tool for rasters
        arcpy.Clip_management(in_raster=in_DEM, out_raster=out_feature_class, in_template_dataset=boundary_shpfn, nodata_value="-3.402823e+038", clipping_geometry="ClippingGeometry", maintain_clipping_extent="NO_MAINTAIN_EXTENT")
        messages.addMessage('Clipped DEM by Boundary')
        
        # Execute PolygonToRaster
        # Set local variables
        outRaster = "C:/temp/studyarearaster.tif"
        field_name = "SA_km2_top-bottom"
        # code for the actual tool to run
        arcpy.PolygonToRaster_conversion(in_features=in_shpfn, value_field=valField, out_rasterdataset=outRaster, cell_assignment="CELL_CENTER", cellsize=cellSize)        
        
#stuff that I have not gotten to work yet Everything above works
        #run the raster calculator starting from the top elevation
        #looping through the shapefile with the field calculator
        #for i in range(elev_range_TE,elev_range_BE,step_freq):
        #looping through the shapefile with the field calculator
        #for i in range(elev_range_BE,elev_range_TE,step_freq):
            #arcpy.gp.RasterCalculator_sa(((in_DEM < i) & (in_DEM >(i+lit_depth)) * outRaster, outfolder)
        
       #looping through the shapefile with the field calculator
        for i in range(elev_range_BE,elev_range_TE,step_freq):
            arcpy.gp.RasterCalculator_sa(((in_DEM < i) & (in_DEM >(i+lit_depth)) * outRaster, outfolder)
            #adding in a field for the SA in the attribute table
            for 
              # Replace a layer/table view name with a path to a dataset (which can be a layer file) or create the layer/table view within the script
                # The following inputs are layers or table views: "1804_1801"
                arcpy.CalculateField_management(in_table="1804_1801", field="SA_KM2_1804-1801", expression="([COUNT]*10*10)/(1000^2)", expression_type="VB", code_block="")
            #Export to csv
                #exporting to a csv file
                def export_to_csv(Output,file_name):
                    with open(file_name,'wb') as out_file:
                        data_writer = csv.writer(out_file)
                        data_writer.writerows(Output)
                export_to_csv(Output,'OGR_output.csv') 
                #arcpy.env.workspace = r'G:\USU_Spring_2015\GIS_Special_Projects\fooling_around\Bringingitalltogether\ClipnCut'
            #arcpy.gp.RasterCalculator_sa("""((outRaster < 1804) & (outRaster >1801)) * outRaster""", "F:/GLEL_Summer_2015/Bear_Lake/Habitat_map/SA_calculation/18000")
            #arcpy.gp.RasterCalculator_sa("""((in_DEM < i) & (in_DEM >(i-lit_depth)) * outRaster""", outfolder) 




#Delete all of the temporary files
        del field_name, out_feature_class
        messages.addMessage('Done!')
        return
